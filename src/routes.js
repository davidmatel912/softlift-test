import Vue from 'vue'
import Router from 'vue-router'
import NotFound from './views/NotFound.vue'
import Preview from './views/Preview.vue'
import HomePage from './views/HomePage.vue'
import Medicare from './views/Medicare.vue'
import Avid from './views/Avid.vue'
import Veteran from './views/Veteran.vue'
import ContinuedMedicare from './views/ContinuedMedicare.vue'
import DNCMedicare from './views/DNCMedicare.vue'
import Offer from './views/Offer.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home-page',
      component: HomePage
    },
    {
      path: '/Medicare',
      name: 'Medicare',
      component: Medicare
    },
    {
      path: '/Avid',
      name: 'Avid',
      component: Avid
    },
    {
      path: '/Veteran',
      name: 'Veteran',
      component: Veteran
    },
    {
      path: '/ContinuedMedicare',
      name: 'ContinuedMedicare',
      component: ContinuedMedicare
    },
    {
      path: '/DNCMedicare',
      name: 'DNCMedicare',
      component: DNCMedicare
    },
    {
      path: '/Offer',
      name: 'Offer',
      component: Offer
    },
    {
      path: '/not-found',
      name: 'not-found',
      component: NotFound
    },
    {
      path: '/preview',
      name: 'preview',
      component: Preview
    },
    {
      path: '*',
      redirect: { name: 'not-found' }
    }
  ],
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})